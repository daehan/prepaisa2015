package implementaciones;

import interfaces.Dibujable;
import herencia.ClaseA;

public class ClaseC extends ClaseA implements Dibujable{

	@Override
	public void dibujar() {
		this.setAtributoA("ATributo de la clase A");
		System.out.println("A");
	}

}
