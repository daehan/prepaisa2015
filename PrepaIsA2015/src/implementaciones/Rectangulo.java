package implementaciones;

import interfaces.Dibujable;

public class Rectangulo implements Dibujable {

	@Override
	public void dibujar() {
		System.out.println();
		System.out.println("*\t*\t*\t*\t*\t*\t*");
		System.out.println("*\t\t\t\t\t\t*");
		System.out.println("*\t\t\t\t\t\t*");
		System.out.println("*\t\t\t\t\t\t*");
		System.out.println("*\t*\t*\t*\t*\t*\t*");
		System.out.println();

	}

}
