package implementaciones;
import interfaces.Dibujable;


public class Main {
	
	private Dibujable dibujables[] = new Dibujable[10];
	
	public void asignar(){
		for (int i = 0; i < dibujables.length/2; i++) {
			dibujables[i] = new Triangulo();
		}
		for (int i = dibujables.length/2; i < dibujables.length; i++) {
			dibujables[i] = new Rectangulo();
		}
	}
	
	public void dibujarTodo(){
		for (int i = 0; i < dibujables.length; i++) {
			dibujables[i].dibujar();
		}
	}

	public static void main(String[] args) {
		Main main = new Main();
		main.asignar();
		main.dibujarTodo();
		Dibujable d = new ClaseC();
		d.dibujar();
	}

}
