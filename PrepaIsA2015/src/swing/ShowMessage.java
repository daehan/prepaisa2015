package swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ShowMessage extends JFrame implements ActionListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static JButton cerrar;
	private static JTextField texto;
	private static JPanel panel;
	
	public ShowMessage() {
		panel = new JPanel();
		setContentPane(panel);
		cerrar = new JButton("Cerrar");
		texto = new JTextField("Nada pasado por parametros");
		setBounds(500, 500, 500, 500);
		panel.add(texto);
		panel.add(cerrar);
		botones();
	}
	
	public void botones(){
		cerrar.addActionListener(this);
	}
	
	public ShowMessage(String text) {
		panel = new JPanel();
		setContentPane(panel);
		cerrar = new JButton("Cerrar");
		this.texto = new JTextField(text);
		setBounds(500, 500, 500, 500);
		panel.add(this.texto);
		panel.add(cerrar);
		botones();
	}
	
	public void setTexto(String texto){
		this.texto = new JTextField(texto);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.cerrar){
			this.dispose();
		}
		
	}
}
