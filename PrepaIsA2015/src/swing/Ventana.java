package swing;

import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

public class Ventana extends JFrame implements ActionListener{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static JPanel panel,panel1,panel2;
	private static JButton boton,boton2;
	private static JTextField texto;
	private static JToggleButton toggle;
	private static JRadioButtonMenuItem radio1, radio2;
	private static JButton cerrar;
	private static JPasswordField password;
	
	
	
	public Ventana(){
		setTitle("Ventana de prueba");
		setBounds(200, 100, 600, 400);
		panel = new JPanel();
		panel1 = new JPanel();
		panel2 = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
		panel2.setLayout(new GridLayout(2, 3, 10, 10));
		texto = new JTextField(200);
		boton = new JButton("Boton Hola");
		boton2 = new JButton("Accion alternativa");
		toggle = new JToggleButton("Booleano");
		radio1 = new JRadioButtonMenuItem("Radio1");
		radio2 = new JRadioButtonMenuItem("Radio 2");
		cerrar = new JButton("Cerrar");
		password = new JPasswordField(32);
		setContentPane(panel);
		panel.add(panel1);
		panel.add(panel2);
		panel2.add(texto);
		panel1.add(boton);
		panel1.add(boton2);
		panel1.add(toggle);
		panel2.add(radio1);
		panel2.add(radio2);
		panel2.add(password);
		panel1.add(cerrar);
		
		botones();
		
		
	}
	
	public void botones(){
		boton.addActionListener(this);
		boton2.addActionListener(this);
		cerrar.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.cerrar){
			this.dispose();
		}
		
		if(e.getSource() == this.boton){
			ShowMessage sm = new ShowMessage("Este texto fue pasado por parametros");
			sm.setVisible(true);
		}
		
		if(e.getSource() == this.boton2){
			ShowMessage sm = new ShowMessage();
			sm.setVisible(true);
		}
	}

	public static void main(String [] args){
		
		Ventana ventana = new Ventana();
		ventana.setVisible(true);
	}
	

}
