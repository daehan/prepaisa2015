package herencia;

public class Main {

	public static void main(String[] args) {
		ClaseA a1,a2;
		ClaseB b;
		
		a1 = new ClaseA();
		b = new ClaseB();
		a2 = new ClaseB();
		a1.setAtributoA("foo1");
		b.setAtributoA("foob1");
		b.setAtributoB("foob2");
		a2.setAtributoA("faa");
		((ClaseB) a2).setAtributoB("Hola");
		
		System.out.println(((ClaseB) a2).getAtributoB());

	}

}
