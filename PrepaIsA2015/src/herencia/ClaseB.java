package herencia;

public class ClaseB extends ClaseA{
	
	private String atributoB;
	
	public ClaseB(){
		super();
		setAtributoB(new String());
	}
	
	public ClaseB(String atributoA, String atributoB){
		super(atributoA);
		this.setAtributoB(atributoB);
	}

	public String getAtributoB() {
		return atributoB;
	}

	public void setAtributoB(String atributoB) {
		this.atributoB = atributoB;
	}

}
