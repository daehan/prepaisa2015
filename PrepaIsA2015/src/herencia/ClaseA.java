package herencia;

public class ClaseA {
	
	private String atributoA;
	
	public ClaseA(){
		atributoA = new String();
	}
	
	public ClaseA(String atributoA){
		this.atributoA = atributoA;
	}
	
	public void setAtributoA(String atributoA){
		this.atributoA = atributoA;
	}

	public String getAtributoA(){
		return atributoA;
	}
}
