
public class Foo {
	
	private String atributo1;
	private Integer atributo2;
	
	public String getAtributo1(){
		return atributo1;
	}
	
	public void setAtributo1(String arg){
		atributo1 = arg;
	}

	public Integer getAtributo2() {
		return atributo2;
	}

	public void setAtributo2(Integer atributo2) {
		this.atributo2 = atributo2;
	}

	public static void main(String[] args) {
		
		Foo f1 = new Foo();
		f1.setAtributo1("Atributo Foo 1");
		f1.setAtributo2(20);
		
		System.out.println(f1.getAtributo1());
		System.out.println(f1.getAtributo2());

	}

}
